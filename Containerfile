# syntax=docker/dockerfile:1.3
FROM docker.io/immawanderer/archlinux:linux-amd64

ARG BUILD_DATE
ARG VCS_REF

LABEL description="Container image for building Fyne (fyne.io) Go projects based on docker.io/immawanderer/archlinux"

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer-containers/archlinux-go-fyne.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=GPL-3.0

# get pkg db, install required pkgs, clear pacman's cache and non-essential
# parts of the OS (needs improvement) and call it a day.
RUN pacman -Sy \
      --noconfirm \
      --needed \
      git \
      gcc \
      go \
      pkgconfig \
      xorg-server-devel \
      libxcursor \
      libxrandr \
      libxinerama \
      libxi \
    && \
    \
    pacman -Scc \
      --noconfirm; \
    \
    \
    rm -rf /usr/share/zoneinfo/*; \
    find /. -name "*~" -type f -delete > /dev/null 2>&1; \
    find /usr/share/terminfo/. -type f -delete \
      ! -name "*xterm*" ! -name "*screen*" ! -name "*screen*"

WORKDIR /

# vim: ft=dockerfile
