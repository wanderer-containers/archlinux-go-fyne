# archlinux-go-fyne
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer-containers/archlinux-go-fyne/status.svg)](https://drone.dotya.ml/wanderer-containers/archlinux-go-fyne)

this repo provides a Containerfile for building [Fyne](http://fyne.io) Go
applications, including all the dependencies.  
based on `docker.io/immawanderer/archlinux`, weekly rebuilt on cron.

### LICENSE
GPL-3.0
